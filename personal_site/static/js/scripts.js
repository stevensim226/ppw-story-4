$(document).ready(function() {
    // Clicked Skill PC
    var clicked_skill

    // Skills PC
    $(".skill-btn").on("click",(e) => {
        skill_name = e.target.parentNode.querySelector("h4").innerText
        $("#skill-name").text(skill_name)
        $(".skill-main-pc").css("background-color","#72b5b7")

        try {
            e.target.parentNode.querySelector(".skill-main-pc").style.backgroundColor = "#633a82"
            clicked_skill = e.target.parentNode.querySelector(".skill-main-pc")
        } catch {
            e.target.parentNode.parentNode.querySelector(".skill-main-pc").style.backgroundColor = "#633a82"
            clicked_skill = e.target.parentNode.parentNode.querySelector(".skill-main-pc")
        }
        switch (skill_name){
            case "HTML/CSS":
                $("#skill-desc").text("Learned HTML and CSS by myself and in Foundations of Programming 1, and Web Programming and Design class at Universitas Indonesia. I have also done several projects using HTML and CSS.")
                break

            case "Javascript":
                $("#skill-desc").text("I am fluent in Javascript and also JQuery to make several projects involving responsive and dynamic websites.")
                break
            
            case "Python":
                $("#skill-desc").text("Learned Python by myself and in Foundations of Programming 1 class at Universitas Indonesia, I am also able to use several frameworks for python such as Requests, Matplotlib, and Flask.")
                break
            
            case "Django":
                $("#skill-desc").text("Learned the Django framework by myself, I am fluent in Django and is also able to create REST APIs through the Django REST Framework.")
                break

            case "React.js":
                $("#skill-desc").text("Learned React by myself, I am also familiar with React Native to create mobile applications. I am also able to connect front end with back end APIs with React.")
                break

            default:
                break
        }
    })

    // Skill PC Hover
    $(".skill-btn").hover(function(e){
        try{
            e.target.parentNode.querySelector(".skill-main-pc").style.backgroundColor = "#633a82"
        }
        catch{}
    }, function(e){
       for (var element of document.querySelectorAll(".skill-main-pc")) {
           if (element !== clicked_skill) {
               element.style.backgroundColor = "#72b5b7"
           }
       }
    })

    // Clicked Mobile Tab
    var opened_tab

    // Skills Mobile
    $(".skill-mobile-btn").on("click",(e) => {
        $(".skill-main").css("backgroundColor","#72b5b7")
        if (opened_tab !== e.target) {
            try{
                e.target.querySelector(".skill-main").style.backgroundColor = "#633a82"
            } catch {
                e.target.parentNode.querySelector(".skill-main").style.backgroundColor = "#633a82"
            }
            opened_tab = e.target
        } else {
            opened_tab = null
        }
        
    })
})