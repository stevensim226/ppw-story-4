from django.urls import path, include
from . import views
from .views import home, others, time

app_name = "story_4"

urlpatterns = [
    path("",views.home,name="home"),
    path("others/",views.others,name="others"),
    path("time/<str:jump>/",views.time,name="time")
]