from django.shortcuts import render, HttpResponse
import datetime

def home(request):
    context = {
        "type" : "home"
    }
    return render(request,"personal_site/index.html",context)

def others(request):
    context = {
        "type" : "others"
    }
    return render(request,"personal_site/others.html",context)

def time(request,jump):
    am_or_pm = (datetime.datetime.now() + datetime.timedelta(hours = int(jump))).strftime("%p")
    context = {
        "time" : datetime.datetime.now() + datetime.timedelta(hours = int(jump)),
        "am_or_pm" : "午前" if am_or_pm == "AM" else "午後"
    }
    return render(request,"personal_site/time.html",context)