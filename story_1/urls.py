from django.urls import path, include
from . import views
from .views import story_1

app_name = "story_1"

urlpatterns = [
    path("",views.story_1,name="home")
]