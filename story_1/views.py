from django.shortcuts import render

def story_1(request):
    context = {
        "type" : "story_1"
    }
    return render(request,"story_1/index.html",context)
