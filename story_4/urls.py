from django.contrib import admin
from django.urls import path, include
from . import views
from .views import blank_link

urlpatterns = [
    path('admin/', admin.site.urls),
    path("story_4/",include("personal_site.urls")),
    path("story_1/",include("story_1.urls")),
    path("",views.blank_link,name="blank")
]
