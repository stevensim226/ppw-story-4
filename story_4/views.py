from django.shortcuts import render

def blank_link(request):
    return render(request,"personal_site/navigation.html")
